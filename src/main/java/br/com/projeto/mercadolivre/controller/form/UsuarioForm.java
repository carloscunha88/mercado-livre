package br.com.projeto.mercadolivre.controller.form;

import br.com.projeto.mercadolivre.model.Usuario;
import br.com.projeto.mercadolivre.validation.EmailUnico;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class UsuarioForm {

    private @NotBlank @Email @EmailUnico String email;
    private @NotBlank @Length(min = 6) String senha;

    public UsuarioForm(@NotBlank @Email String email, @NotBlank String senha) {
        this.email = email;
        this.senha = senha;
    }

    public Usuario toUsuario() {
        return new Usuario(email, senha);
    }

    @Override
    public String toString() {
        return "UsuarioForm[" +
                "email='" + email + '\'' +
                ", senha='" + senha + '\'' +
                ']';
    }
}
