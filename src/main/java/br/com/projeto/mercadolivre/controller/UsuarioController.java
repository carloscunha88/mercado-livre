package br.com.projeto.mercadolivre.controller;

import br.com.projeto.mercadolivre.controller.form.UsuarioForm;
import br.com.projeto.mercadolivre.model.Usuario;
import br.com.projeto.mercadolivre.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioRepository repository;

    @PostMapping(path = "/usuarios")
    public String salvar(@RequestBody @Valid UsuarioForm request) {
        Usuario novoUsuario = request.toUsuario();
        repository.save(novoUsuario);
        return novoUsuario.getEmail();
    }
}
