package br.com.projeto.mercadolivre.controller;

import br.com.projeto.mercadolivre.model.Categoria;
import br.com.projeto.mercadolivre.repository.CategoriaRepository;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

public class CategoriaForm {

    private CategoriaRepository repository;

    private @NotBlank String nome;
    private String categoriaMae;

    public Categoria toCategoria(CategoriaRepository repository) {
        Categoria categoria = new Categoria(nome);
        if(!categoriaMae.isEmpty()) {
            Categoria categoriaPrincipal = repository.findByNome(categoriaMae);
            Assert.notNull(categoriaPrincipal, "Nome da categoriaMae invalido");
            categoria.setCategoriaMae(categoriaPrincipal);
        }
        return categoria;
    }

    public String getNome() {
        return nome;
    }

    public String getCategoriaMae() {
        return categoriaMae;
    }

    @Override
    public String toString() {
        return "CategoriaForm{" +
                "nome='" + nome + '\'' +
                ", categoriaMae='" + categoriaMae + '\'' +
                '}';
    }
}
