package br.com.projeto.mercadolivre.controller;

import br.com.projeto.mercadolivre.model.Categoria;
import br.com.projeto.mercadolivre.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/categorias")
public class CategoriaController {

    @Autowired
    private CategoriaRepository repository;

    @PostMapping
    public String salvar(@RequestBody @Valid CategoriaForm request) {
        try{
            Categoria novaCategoria = request.toCategoria(repository);
            repository.save(novaCategoria);
            return repository.findByNome(request.getNome()).toString();
        }catch (Exception e) {
            return e.getMessage();
        }
    }
}
