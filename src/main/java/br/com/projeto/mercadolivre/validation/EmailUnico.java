package br.com.projeto.mercadolivre.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {EmailUnicoValidator.class})
@Target({FIELD, PARAMETER})
@Retention(RUNTIME)
public @interface EmailUnico {

    String message() default "E-mail ja esta cadastrado no sistema";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
