package br.com.projeto.mercadolivre.validation;

import br.com.projeto.mercadolivre.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailUnicoValidator implements ConstraintValidator<EmailUnico, String> {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean hasEmail = repository.existsByEmail(value);
        return !hasEmail;
    }
}
