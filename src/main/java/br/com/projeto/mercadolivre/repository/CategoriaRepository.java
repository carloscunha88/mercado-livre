package br.com.projeto.mercadolivre.repository;

import br.com.projeto.mercadolivre.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

    Optional<Categoria> findByCategoriaMae_Nome(String nome);

    Categoria findByNome(String nome);
}
